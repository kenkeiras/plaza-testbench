#!/usr/bin/env bash

COMMIT="$1"
TESTID=$RANDOM$RANDOM$RANDOM
DURATION=${DURATION:-120}
BASELINE=${BASELINE:-1000}

if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]] || [[  -z "$COMMIT" ]];then
    echo 'Usage:  bash benchmark-commit.sh [<commit>|<branch>]'
    exit 0
fi

set -euo pipefail

echo "Check CLI tool dependencies"
pushd cli

go build # Tool can be built

# Additional utils are present
echo {} | jq >> /dev/null

popd

echo "Launching backend"
git submodule update --init --recursive .
pushd core/backend

git fetch --all
git checkout "$COMMIT"

COMMITID=`git rev-parse --short HEAD`

docker build -t "pm-backend:$COMMITID" .

DOCKERNAME="pm-backend-benchmark-${TESTID}"
echo "Running as $DOCKERNAME"

docker run -d --name "$DOCKERNAME" "pm-backend:$COMMITID"

popd

cleanup() {
    echo "Cleaning up..."

    set -x
    docker rm -f "$DOCKERNAME"
}

set +eu

(
    set -euo pipefail

    IP=`docker exec -it "$DOCKERNAME" hostname -i| tr -d '\n\r'`
    pushd cli

    export PLAZA_ROOT="http://$IP:8888"

    for i in `seq 1 60`;do
        curl -s "$PLAZA_ROOT"/api/v0/ping >> /dev/null && break || sleep 1
    done

    USERNAME=test_$TESTID
    PASSWD=test_$TESTID

    ./cli register -name $USERNAME -password $PASSWD -email $USERNAME@test.com
    TOKEN=`./cli login -name $USERNAME -password $PASSWD|jq -r .token`

    CONTROL_URL=`./cli add-bridge -user_name $USERNAME -token "$TOKEN" -bridge_name $USERNAME | jq -r .control_url`

    # Start benchmark per-se
    RESULT=`./cli benchmark -duration "$DURATION" -sender_freq "$BASELINE" -token "$TOKEN" -url "${CONTROL_URL}"`
    echo "Result: $RESULT"
)
x=$?

if [ $x -ne 0 ];then
    LOGFILE=benchmark-$TESTID.log
    docker logs "$DOCKERNAME" > "$LOGFILE"
    echo "Saved logs to $LOGFILE"
fi

cleanup

exit $x

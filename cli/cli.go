package main

import (
	"flag"
	"fmt"
	"io"

	add_bridge "./commands/add_bridge"
	benchmark "./commands/benchmark"
	login "./commands/login"
	register "./commands/register"
	get_token "./commands/get_bridge_token"
	debug "./commands/debug"
)

// Exit codes are int values that represent an exit code for a particular error.
const (
	ExitCodeOK    int = 0
	ExitCodeError int = 1 + iota
)

// CLI is the command line object
type CLI struct {
	// outStream and errStream are the stdout and stderr
	// to write message from the CLI.
	outStream, errStream io.Writer
}

func showHelp(arg0 string) {
	fmt.Printf("Usage: %v <command> <command options>\n", arg0)
	fmt.Println("  Commands:")
	fmt.Println("    register: Register an user on a Plaza instance")
	fmt.Println("    login   : Login an user on a Plaza instance, get an API token")
	fmt.Println("    add-bridge: Register a bridge, get it's Websocket URL")
	fmt.Println("    benchmark : Benchmark a Plaza instance")
	fmt.Println("    get-bridge-token : Get a bridge token")

	fmt.Println("")
	fmt.Println("  Debugging utils:")
	fmt.Println("    establish-connection: Connect to a Plaza instance and sit on it until it's disconnected")
}

// Run invokes the CLI with the given arguments.
func (cli *CLI) Run(args []string) int {
	registerCmd := flag.NewFlagSet("register", flag.ExitOnError)
	registerParams := register.SetParameters(registerCmd)

	loginCmd := flag.NewFlagSet("login", flag.ExitOnError)
	loginParams := login.SetParameters(loginCmd)

	addBridgeCmd := flag.NewFlagSet("add-bridge", flag.ExitOnError)
	addBridgeParams := add_bridge.SetParameters(addBridgeCmd)

	benchmarkCmd := flag.NewFlagSet("benchmark", flag.ExitOnError)
	benchmarkParams := benchmark.SetParameters(benchmarkCmd)

	getTokenCmd := flag.NewFlagSet("get-bridge-token", flag.ExitOnError)
	getTokenParams := get_token.SetParameters(getTokenCmd)

	establishConnectionCmd := flag.NewFlagSet("establish-connection", flag.ExitOnError)
	establishConnectionParams := debug.SetEstablishConnectionParameters(establishConnectionCmd)

	if len(args) < 2 {
		showHelp(args[0])
		return ExitCodeError
	}

	switch args[1] {
	// Commands
	case "register":
		err := register.CheckParameters(registerCmd, registerParams, args[2:])
		if err != nil {
			return ExitCodeError
		}
		return register.Register(registerParams)

	case "login":
		err := login.CheckParameters(loginCmd, loginParams, args[2:])
		if err != nil {
			return ExitCodeError
		}
		return login.Login(loginParams)

	case "add-bridge":
		err := add_bridge.CheckParameters(addBridgeCmd, addBridgeParams, args[2:])
		if err != nil {
			return ExitCodeError
		}
		return add_bridge.AddBridge(addBridgeParams)

	case "benchmark":
		err := benchmark.CheckParameters(benchmarkCmd, benchmarkParams, args[2:])
		if err != nil {
			return ExitCodeError
		}
		return benchmark.Benchmark(benchmarkParams)

	case "get-bridge-token":
		err := get_token.CheckParameters(getTokenCmd, getTokenParams, args[2:])
		if err != nil {
			return ExitCodeError
		}
		return get_token.GetToken(getTokenParams)

	case "establish-connection":
		err := debug.CheckEstablishConnectionParameters(establishConnectionCmd, establishConnectionParams, args[2:])
		if err != nil {
			return ExitCodeError
		}
		return debug.EstablishConnection(establishConnectionParams)

		// Help
	case "-help":
		showHelp(args[0])
		return ExitCodeOK
	case "--help":
		showHelp(args[0])
		return ExitCodeOK
	case "-h":
		showHelp(args[0])
		return ExitCodeOK

		// Unknown command
	default:
		fmt.Printf("Unexpected command \"%v\"\n", args[1])
		showHelp(args[0])
		return ExitCodeError
	}

	return ExitCodeOK
}

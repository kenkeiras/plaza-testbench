package config

import (
	"os"
	"strings"
)

func GetApiRoot() string {
	return GetRoot() + "/api/v0"
}

func GetRoot() string {
	return strings.TrimRight(getEnvWithDefault("PLAZA_ROOT", "http://localhost:8888/"), "/")
}

func getEnvWithDefault(env string, defaultValue string) string {
	value := os.Getenv(env)
	if value != "" {
		return value
	}
	return defaultValue
}

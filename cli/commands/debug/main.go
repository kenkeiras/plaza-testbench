package debug

import (
	config "../../config"
	"errors"
	"flag"
	"fmt"
	"net/url"
	"os"
	"strings"
	"github.com/gorilla/websocket"
	"net/http"
	"time"
)

// Parameter control
type establishConnectionParams struct {
	control_url *string
	token       *string
}

func SetEstablishConnectionParameters(cmd *flag.FlagSet) establishConnectionParams {
	return establishConnectionParams{
		control_url: cmd.String("url", "", "Control URL"),
		token:       cmd.String("token", "", "Authentication token"),
	}
}

func checkAllEstablishConnectionSet(params establishConnectionParams) ([]string, error) {
	var ValueUnset = errors.New("unset values")
	unsetValues := make([]string, 2)
	unsetCount := 0

	if *(params.control_url) == "" {
		unsetValues[unsetCount] = "url"
		unsetCount += 1
	}

	if *(params.token) == "" {
		unsetValues[unsetCount] = "token"
		unsetCount += 1
	}

	errorCode := ValueUnset
	if unsetCount == 0 {
		errorCode = nil
	}

	return unsetValues, errorCode
}

func CheckEstablishConnectionParameters(cmd *flag.FlagSet, params establishConnectionParams, args []string) error {
	err := cmd.Parse(args)
	if err != nil {
		return err
	}

	unsetValues, err := checkAllEstablishConnectionSet(params)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Required values not set: %v\n", unsetValues)
		cmd.PrintDefaults()
		return err
	}

	return nil
}

func EstablishConnection(params establishConnectionParams) int {
	endpoint := strings.Trim(
		strings.Replace(config.GetRoot(), "http", "ws", 1)+*(params.control_url),
		"/")

	// Remove "communication" at the end to get to the bridge root URL
	fmt.Fprintf(os.Stderr, "# Connection endpoint: %s\n", endpoint)
	_, err := url.Parse(endpoint)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error parsing endpoint: %v\n", err)
		return 1
	}

	c, _, err := websocket.DefaultDialer.Dial(endpoint, http.Header{})
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error establishing connection: %v\n", err)
		return 1
	}

	{
		authentication := fmt.Sprintf(`{ "type": "AUTHENTICATION", "value": { "token": "%s" } }`, *(params.token))
		err = c.WriteMessage(websocket.TextMessage, []byte(authentication))
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error before authenticating")
			return 1
		}
	}

	{
		config := `{ "type": "CONFIGURATION", "value": { "blocks": [], "is_public": false, "service_name": "cli-tester" } }`
		err = c.WriteMessage(websocket.TextMessage, []byte(config))
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error establishing configuration")
			return 1
		}
	}

	go func () {
		for {
			time.Sleep(15 * time.Second)
			err := c.WriteMessage(websocket.PingMessage, []byte{})
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error on ping keepalive")
			}
		}
	}()

	for {
		_, message, err := c.ReadMessage()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Connection closed: %v\n", err)
			return 0
		}

		fmt.Fprintf(os.Stderr, "%s\n", message)
	}
}

package add_bridge

import (
	config "../../config"
	"errors"
	"flag"
	"fmt"
	"github.com/levigross/grequests"
	"os"
)

type getTokenParams struct {
	token       *string
	bridge_id *string
	token_name *string
}

func SetParameters(cmd *flag.FlagSet) getTokenParams {
	return getTokenParams{
		token:       cmd.String("token", "", "API token"),
		bridge_id:   cmd.String("bridge_id", "", "Bridge id"),
		token_name:  cmd.String("token_name", "", "Token name"),
	}
}

func checkAllSet(params getTokenParams) ([]string, error) {
	var ValueUnset = errors.New("unset values")
	unsetValues := make([]string, 3)
	unsetCount := 0

	if *(params.token) == "" {
		unsetValues[unsetCount] = "token"
		unsetCount += 1
	}
	if *(params.bridge_id) == "" {
		unsetValues[unsetCount] = "bridge_id"
		unsetCount += 1
	}
	if *(params.token_name) == "" {
		unsetValues[unsetCount] = "token_name"
		unsetCount += 1
	}

	errorCode := ValueUnset
	if unsetCount == 0 {
		errorCode = nil
	}

	return unsetValues, errorCode
}

func CheckParameters(cmd *flag.FlagSet, params getTokenParams, args []string) error {
	err := cmd.Parse(args)
	if err != nil {
		return err
	}

	unsetValues, err := checkAllSet(params)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Required values not set: %v\n", unsetValues)
		cmd.PrintDefaults()
		return err
	}

	return nil
}

func GetToken(params getTokenParams) int {
	endpoint := fmt.Sprintf("%v/bridges/by-id/%s/tokens", config.GetApiRoot(), *(params.bridge_id))
	fmt.Fprintf(os.Stderr, "# Add-bridge endpoint: %s\n", endpoint)

	resp, err := grequests.Post(endpoint,
		&grequests.RequestOptions{
			JSON: map[string]string{
				"name": *(params.token_name),
			},
			Headers: map[string]string{
				"Authorization": fmt.Sprintf("%v", *(params.token)),
			},
		})

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", resp.Error)
		return 1
	}

	if resp.Ok != true {
		fmt.Fprintf(os.Stderr, "HTTP Error %v: %s\n", resp.StatusCode, resp.String())
		return 2
	} else {
		fmt.Println(resp.String())
	}

	return 0
}

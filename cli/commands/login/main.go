package login

import (
	config "../../config"
	"errors"
	"flag"
	"fmt"
	"github.com/levigross/grequests"
	"os"
)

type loginParams struct {
	username *string
	password *string
}

func SetParameters(cmd *flag.FlagSet) loginParams {
	return loginParams{
		username: cmd.String("name", "", "User name"),
		password: cmd.String("password", "", "User password"),
	}
}

func checkAllSet(params loginParams) ([]string, error) {
	var ValueUnset = errors.New("unset values")
	unsetValues := make([]string, 2)
	unsetCount := 0

	if *(params.username) == "" {
		unsetValues[unsetCount] = "name"
		unsetCount += 1
	}
	if *(params.password) == "" {
		unsetValues[unsetCount] = "password"
		unsetCount += 1
	}

	errorCode := ValueUnset
	if unsetCount == 0 {
		errorCode = nil
	}

	return unsetValues, errorCode
}

func CheckParameters(cmd *flag.FlagSet, params loginParams, args []string) error {
	err := cmd.Parse(args)
	if err != nil {
		return err
	}

	unsetValues, err := checkAllSet(params)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Required values not set: %v\n", unsetValues)
		cmd.PrintDefaults()
		return err
	}

	return nil
}

func Login(params loginParams) int {
	endpoint := config.GetApiRoot() + "/sessions/login"
	fmt.Fprintf(os.Stderr, "# Login endpoint: %s\n", endpoint)

	resp, err := grequests.Post(endpoint,
		&grequests.RequestOptions{
			JSON: map[string]string{
				"password": *(params.password),
				"username": *(params.username),
			},
		})

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", resp.Error)
		return 1
	}

	if resp.Ok != true {
		fmt.Fprintf(os.Stderr, "HTTP Error %v: %s\n", resp.StatusCode, resp.String())
		return 2
	} else {
		fmt.Println(resp.String())
	}

	return 0
}

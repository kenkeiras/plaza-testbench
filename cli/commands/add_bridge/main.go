package add_bridge

import (
	config "../../config"
	"errors"
	"flag"
	"fmt"
	"github.com/levigross/grequests"
	"os"
)

type addBridgeParams struct {
	token       *string
	user_name   *string
	bridge_name *string
}

func SetParameters(cmd *flag.FlagSet) addBridgeParams {
	return addBridgeParams{
		token:       cmd.String("token", "", "API token"),
		user_name:   cmd.String("user_name", "", "User name"),
		bridge_name: cmd.String("bridge_name", "", "Bridge name"),
	}
}

func checkAllSet(params addBridgeParams) ([]string, error) {
	var ValueUnset = errors.New("unset values")
	unsetValues := make([]string, 3)
	unsetCount := 0

	if *(params.token) == "" {
		unsetValues[unsetCount] = "token"
		unsetCount += 1
	}
	if *(params.user_name) == "" {
		unsetValues[unsetCount] = "user_name"
		unsetCount += 1
	}
	if *(params.bridge_name) == "" {
		unsetValues[unsetCount] = "bridge_name"
		unsetCount += 1
	}

	errorCode := ValueUnset
	if unsetCount == 0 {
		errorCode = nil
	}

	return unsetValues, errorCode
}

func CheckParameters(cmd *flag.FlagSet, params addBridgeParams, args []string) error {
	err := cmd.Parse(args)
	if err != nil {
		return err
	}

	unsetValues, err := checkAllSet(params)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Required values not set: %v\n", unsetValues)
		cmd.PrintDefaults()
		return err
	}

	return nil
}

func AddBridge(params addBridgeParams) int {
	endpoint := fmt.Sprintf("%v/users/%v/bridges/", config.GetApiRoot(), *(params.user_name))
	fmt.Fprintf(os.Stderr, "# Add-bridge endpoint: %s\n", endpoint)

	resp, err := grequests.Post(endpoint,
		&grequests.RequestOptions{
			JSON: map[string]string{
				"name": *(params.bridge_name),
			},
			Headers: map[string]string{
				"Authorization": fmt.Sprintf("%v", *(params.token)),
			},
		})

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", resp.Error)
		return 1
	}

	if resp.Ok != true {
		fmt.Fprintf(os.Stderr, "HTTP Error %v: %s\n", resp.StatusCode, resp.String())
		return 2
	} else {
		fmt.Println(resp.String())
	}

	return 0
}

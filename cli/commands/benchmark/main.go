package benchmark

import (
	config "../../config"
	"container/list"
	"errors"
	"flag"
	"fmt"
	"github.com/gorilla/websocket"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"
)

// Parameter control
type benchmarkParams struct {
	control_url *string
	token       *string
	duration    *int
	sender_freq *int
}

func SetParameters(cmd *flag.FlagSet) benchmarkParams {
	return benchmarkParams{
		control_url: cmd.String("url", "", "Control URL"),
		token:       cmd.String("token", "", "Authentication token"),
		duration:    cmd.Int("duration", 10, "Duration (in seconds)"),
		sender_freq: cmd.Int("sender_freq", 100, "Sender frequency (in Hz)"),
	}
}

func checkAllSet(params benchmarkParams) ([]string, error) {
	var ValueUnset = errors.New("unset values")
	unsetValues := make([]string, 2)
	unsetCount := 0

	if *(params.control_url) == "" {
		unsetValues[unsetCount] = "url"
		unsetCount += 1
	}

	if *(params.token) == "" {
		unsetValues[unsetCount] = "token"
		unsetCount += 1
	}

	errorCode := ValueUnset
	if unsetCount == 0 {
		errorCode = nil
	}

	return unsetValues, errorCode
}

func CheckParameters(cmd *flag.FlagSet, params benchmarkParams, args []string) error {
	err := cmd.Parse(args)
	if err != nil {
		return err
	}

	if *(params.duration) <= 0 {
		fmt.Fprintf(os.Stderr, "Duration (now set to: %v) must be > 0\n", *(params.duration))
		cmd.PrintDefaults()
		return errors.New("Non-positive duration value")
	}

	if *(params.sender_freq) <= 0 {
		fmt.Fprintf(os.Stderr, "Sender frequency (now set to: %v) must be > 0\n", *(params.sender_freq))
		cmd.PrintDefaults()
		return errors.New("Non-positive sender frequency value")
	}

	unsetValues, err := checkAllSet(params)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Required values not set: %v\n", unsetValues)
		cmd.PrintDefaults()
		return err
	}

	return nil
}

// Benchmark logic
const (
	Ready       int = iota
	Error       int = iota
	Stop        int = iota
	StartBridge int = iota
	SetRate     int = iota
	GetRate     int = iota
)

type BenchmarkControl struct {
	code  int
	value int
	error error
}

type BenchmarkClientReport struct {
	elapsed int64
}

type BenchmarkBridgeReport struct {
}

type BenchmarkStatusReport struct {
	max       int64
	min       int64
	sum       float64
	count     int
	on_flight int
	time      int64
}

func Benchmark(params benchmarkParams) int {
	endpoint := strings.Trim(
		strings.Replace(config.GetRoot(), "http", "ws", 1)+*(params.control_url),
		"/")

	// Remove "communication" at the end to get to the bridge root URL
	rootIndex := len(endpoint) - len("communication")
	if strings.LastIndex(endpoint, "communication") == rootIndex {
		endpoint = endpoint[0:rootIndex]
	}

	fmt.Fprintf(os.Stderr, "# Benchmark endpoint: %s\n", endpoint)
	url, err := url.Parse(endpoint)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %v", err)
		return 1
	}

	processControl := make(chan BenchmarkControl)
	bridgeControl := make(chan BenchmarkControl)
	bridgeReport := make(chan BenchmarkBridgeReport)
	clientControl := make(chan BenchmarkControl)
	clientReport := make(chan BenchmarkClientReport)
	statusReport := make(chan BenchmarkStatusReport)

	// Basic benchmark is:
	//   1 bridge + 1 client
	//
	// How much data can the bridge pass to the client?

	// Launch participants
	rate := float32(*(params.sender_freq))
	refTime := time.Now()

	go benchmarkingBridge(url, *(params.token), bridgeControl, bridgeReport, refTime, rate)
	_ = <-bridgeControl
	// fmt.Println("Bridge said:", message)

	go benchmarkingClient(url, *(params.token), clientControl, clientReport, refTime)
	_ = <-clientControl
	// fmt.Println("Client said:", message)

	go benchmarkingReporter(bridgeReport, clientReport, statusReport)

	bridgeControl <- BenchmarkControl{code: StartBridge, value: -1, error: nil}

	// Prepare timer for finish
	TotalTime := time.Duration(*(params.duration)) * time.Second
	go func() {
		time.Sleep(TotalTime)
		processControl <- BenchmarkControl{code: Stop, value: -1, error: nil}
	}()

	increases := list.New()
	decreases := list.New()
	last_increased := false
	last_decreased := false

	// Show stats
	for {
		select {
		// Control
		case _ = <-processControl:
			// Find appropriate rate
			lower, upper := get_conclusion_bounds(increases, decreases)
			fmt.Fprintf(os.Stderr, "\nLower bound: %.2f | Upper bound: %.2f\n", lower, upper)
			fmt.Printf("{ \"lower\": %.2f, \"upper\": %.2f }\n", lower, upper)

			return 0 // TODO: Check that the returned code is stop
		case report := <-bridgeControl:
			if (report.code == Stop) || (report.code == Error) {
				return 1 // TODO: Check that the returned code is stop
			} else if report.code == GetRate {
				bridgeControl <- BenchmarkControl{code: SetRate, value: int(rate), error: nil}
			}
		case _ = <-clientControl:
			return 2 // TODO: Check that the returned code is stop

		case report := <-statusReport:
			// Calculate the rate at which the messages are arriving
			//  we cannot use the setted one for bond calculation, as
			//  there's a difference from theoreticall set to actually sent.
			real_rate := float32(float64(report.count) / (float64(report.time) / 1.e9))

			printReport(report)
			new_rate := adjust_rate(rate, report)

			if new_rate > rate {
				increases.PushBack(real_rate)
				last_decreased = false
				if !last_increased {
					fmt.Fprintln(os.Stderr, "* Control: Increasing")
					rate = new_rate
					last_increased = true
				} else {
					last_increased = false
				}
			} else if new_rate < rate {
				decreases.PushBack(real_rate)
				last_increased = false
				if !last_decreased {
					fmt.Fprintln(os.Stderr, "* Control: Reducing")
					rate = new_rate
					last_decreased = true
				} else {
					last_decreased = false
				}
			} else {
				last_increased = false
				last_decreased = false
			}
		}
	}

	return 0
}

func get_conclusion_bounds(increasesL *list.List, decreasesL *list.List) (float32, float32) {
	// Conclusion bounds are
	//  Lower: max increasing with no decreasing
	//  Upper: min decreasing with no increasing
	var upper_bound, lower_bound float32

	index := 0

	// Find max increasing
	var max_increasing float32
	for e := increasesL.Front(); e != nil; e = e.Next() {
		val := e.Value.(float32)

		if index == 0 {
			max_increasing = val
		} else {
			if val > max_increasing {
				max_increasing = val
			}
		}
		index += 1
	}

	// Find min_decreasing and upper bound
	index = 0
	upper_bound = -1
	var min_decreasing float32
	for e := decreasesL.Front(); e != nil; e = e.Next() {
		val := e.Value.(float32)
		if index == 0 {
			min_decreasing = val
		} else {
			if val < min_decreasing {
				min_decreasing = val
			}
		}

		if ((upper_bound < 0) || (val < upper_bound)) && (val > max_increasing) {
			upper_bound = val
		}

		index += 1
	}

	// Find lower bound
	lower_bound = -1
	for e := increasesL.Front(); e != nil; e = e.Next() {
		val := e.Value.(float32)
		if ((lower_bound < 0) || (val > lower_bound)) && (val < min_decreasing) {
			lower_bound = val
		}
	}

	return lower_bound, upper_bound
}

func adjust_rate(freq float32, report BenchmarkStatusReport) float32 {
	avg := report.sum / float64(report.count) // In NS
	if avg > 100e6 {                          // Avg > 100ms
		freq -= freq / 10 // -10%
	}
	if (avg < 10e6) && (report.on_flight < int(freq/100)) { // Avg < 1ms, on-flight < 1%
		freq += freq / 10 // +10%
	}

	if freq < 1 {
		return 1
	} else {
		return freq
	}
}

func printReport(report BenchmarkStatusReport) {
	fmt.Fprintf(os.Stderr, "%.2f msg/s, in-flight: %v, avg: %.2fms longest: %.2fms, shortest: %.2fms\x1b[0m\n",
		float64(report.count)/(float64(report.time)/1.e9), report.on_flight,
		float64(report.sum)/float64(report.count*1e6), float64(report.max)/1e6, float64(report.min)/1e6,
	)
}

func benchmarkingReporter(bridgeReport chan BenchmarkBridgeReport, clientReport chan BenchmarkClientReport, statusReport chan BenchmarkStatusReport) {
	ReportInterval := 1 * time.Second
	on_flight := 0

	count := 0
	var max, min int64
	var sum float64

	IntervalStart := time.Now()
	for {
		select {
		// Receive reports
		case <-bridgeReport:
			on_flight += 1

		case report := <-clientReport:
			on_flight -= 1
			elapsed := report.elapsed
			if count == 0 {
				max = elapsed
				min = elapsed
				count = 1
				sum = float64(elapsed)
			} else {
				if elapsed > max {
					max = elapsed
				}
				if elapsed < min {
					min = elapsed
				}
				count++
				sum += float64(elapsed)
			}

		case <-time.After(ReportInterval - (time.Now().Sub(IntervalStart))):
			// Report time
			statusReport <- BenchmarkStatusReport{
				min:       min,
				max:       max,
				count:     count,
				sum:       sum,
				time:      time.Now().Sub(IntervalStart).Nanoseconds(),
				on_flight: on_flight,
			}
			min = 0
			max = 0
			sum = 0
			count = 0
			IntervalStart = time.Now()
		}
	}
}

func benchmarkingBridge(url *url.URL, token string, control chan BenchmarkControl, report chan BenchmarkBridgeReport, refTime time.Time, rate float32) {

	c, _, err := websocket.DefaultDialer.Dial(url.String()+"communication",
		http.Header{"Authorization": {token}},
	)

	if err != nil {
		control <- BenchmarkControl{code: Error, error: err, value: -1}
		return
	}

	defer c.Close()

	config := `{ "type": "CONFIGURATION", "value": { "blocks": [], "is_public": false, "service_name": "cli-tester" } }`
	err = c.WriteMessage(websocket.TextMessage, []byte(config))
	if err != nil {
		fmt.Fprintln(os.Stderr, "error:", err)
		return
	}
	fmt.Fprintln(os.Stderr, "Bridge configured")
	control <- BenchmarkControl{code: Ready, error: nil, value: -1}
	answer := <-control
	fmt.Fprintln(os.Stderr, "→ Bridge starting", answer)

	SleepInterval := time.Duration(1e9/rate) * time.Nanosecond
	ReloadInterval := 1 * time.Second

	ReportStart := time.Now()

	for {
		t0 := time.Now()
		message := fmt.Sprintf(
			`{ "type": "NOTIFICATION", "key": "benchmark", "to_user": null, "content": "none", "value": ">>>>>%v<<<<<" }`,
			t0.Sub(refTime).Nanoseconds())
		err = c.WriteMessage(websocket.TextMessage, []byte(message))
		if err != nil {
			fmt.Fprintln(os.Stderr, "bridge error:", err)
			control <- BenchmarkControl{code: Error, error: err, value: -1}
			return
		}

		report <- BenchmarkBridgeReport{}
		if time.Now().Sub(ReportStart) > ReloadInterval {
			control <- BenchmarkControl{code: GetRate, error: nil, value: -1}
			response := <-control
			if response.code != SetRate {
				panic("Expected SetRate")
			}
			newRate := response.value
			SleepInterval = time.Duration(1e9/newRate) * time.Nanosecond
		}

		time.Sleep(SleepInterval)
	}
}

func benchmarkingClient(url *url.URL, token string, control chan BenchmarkControl, report chan BenchmarkClientReport, refTime time.Time) {
	c, _, err := websocket.DefaultDialer.Dial(url.String()+"signals",
		http.Header{"Authorization": {token}},
	)

	if err != nil {
		fmt.Fprintln(os.Stderr, "error:", err)
		control <- BenchmarkControl{code: Error, error: err, value: -1}
		return
	}

	fmt.Fprintln(os.Stderr, "Client connected")

	control <- BenchmarkControl{code: Ready, error: nil, value: -1}

	defer c.Close()
	PingInterval := 10 * time.Second
	PingIntervalStart := time.Now()

	for {
		_, message, err := c.ReadMessage()
		if err != nil {
			fmt.Fprintln(os.Stderr, "client error:", err)
			control <- BenchmarkControl{code: Error, error: err, value: -1}
			return
		}
		tReceived := time.Since(refTime)
		msg := string(message)
		result := msg[strings.Index(msg, ">>>>>")+5 : strings.Index(msg, "<<<<<")]

		tSent, err := strconv.ParseInt(result, 10, 64)
		if err != nil {
			fmt.Fprintf(os.Stderr, "While parsing: %v\n", result)
			panic(err)
		}
		elapsed := tReceived.Nanoseconds() - tSent // Nanoseconds to Milliseconds

		if elapsed < 0 {
			fmt.Fprintf(os.Stderr, "??? %v\n  - In: %v (%v)\n  - Recv: %v\n", msg, result, tSent, tReceived.Nanoseconds())
		}
		if time.Since(PingIntervalStart) > PingInterval {
			// Send ping
			go c.WriteControl(websocket.PingMessage, []byte(""), time.Now().Add(PingInterval))
			PingIntervalStart = time.Now()
		}

		report <- BenchmarkClientReport{elapsed: elapsed}
	}
}

# Plaza testbench

Assorted tools to simplify the testing and maintenance of the [Plaza](https://projectplaza.space/) project.

## CLI

Golang-based command line interface to interact with Plaza deployments.
